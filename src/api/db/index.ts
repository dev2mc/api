import { Connection, createConnection } from 'mongoose'

let conn: Connection

const uri: string | undefined = process.env.DB_PATH

export const getConnection = async (): Promise<Connection> => {
  // Because `conn` is in the global scope, Lambda may retain it between
  // function calls thanks to `callbackWaitsForEmptyEventLoop`.
  // This means your Lambda function doesn't have to go through the
  // potentially expensive process of connecting to MongoDB every time.

  if (conn == null) {
    if (uri === undefined) throw new Error('DB_PATH missing')
    conn = await createConnection(uri, {
      // Buffering means mongoose will queue up operations if it gets
      // disconnected from MongoDB and send them when it reconnects.
      // With serverless, better to fail fast if not connected.

      bufferCommands: false, // Disable mongoose buffering
      bufferMaxEntries: 0, // and MongoDB driver buffering
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    })
  }

  return conn
}
