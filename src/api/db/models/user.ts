import { Document, SchemaDefinition, SchemaTypes, Schema, Connection, Model } from 'mongoose'

export interface IUser extends Document {
  name: string
}

const schema: SchemaDefinition = {
  name: {
    type: SchemaTypes.String,
    required: true
  }
}

const collectionName: string = 'users'
const userSchema: Schema = new Schema(schema)

const User = (conn: Connection): Model<IUser> => conn.model(collectionName, userSchema)

export default User
