import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import { createServer } from 'http'
import compression from 'compression'
import cors from 'cors'
import helmet from 'helmet'
import { typeDefs, resolvers } from './graphql/schema'
import { getConnection } from './db'

const app = express()
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async () => {
    const dbConn = await getConnection()
    return { dbConn }
  },
  playground: true,
  introspection: true
})

app.use('*', cors())
app.use(compression())
app.use(helmet())

server.applyMiddleware({
  app,
  path: '/graphql'
})

const httpServer = createServer(app)

httpServer.listen({ port: 3000 }, (): void =>
  console.log(`\n🚀  GraphQL is now running on http://localhost:3000/graphql`)
)
