import { gql } from 'apollo-server-express'

import userSchema from './users'
import customSchema from './custom'

const linkSchema = gql`
  type Query {
    _: Boolean
  }

  type Mutation {
    _: Boolean
  }
`

export default [
  linkSchema,
  userSchema,
  customSchema
]
