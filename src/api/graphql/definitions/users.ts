import { gql } from 'apollo-server-express'

export default gql`
  extend type Query {
    getAllUsers: [User!]
    getUser(_id: ID!): User
  }

  type User {
    _id: ID!
    name: String!
  }
`
