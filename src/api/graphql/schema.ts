import typeDefs from './definitions'
import resolvers from './resolvers'

export {
  typeDefs,
  resolvers
}
