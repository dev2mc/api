import { Model, Connection } from 'mongoose'
import { ApolloError } from 'apollo-server-express'
import UserModel, { IUser } from '../../db/models/user'

export default {
  Query: {
    getAllUsers: async (_: void, args: void, { dbConn }: { dbConn: Connection }): Promise<IUser[]> => {
      const User: Model<IUser> = UserModel(dbConn)

      let list: IUser[]

      try {
        list = await User.find().exec()
      } catch (error) {
        console.error('> getAllUsers error: ', error)

        throw new ApolloError('Error retrieving all users')
      }

      return list
    },

    getUser: async (_: void, { _id }: { _id: IUser['_id'] }, { dbConn }: { dbConn: Connection }): Promise<IUser | null> => {
      const User: Model<IUser> = UserModel(dbConn)

      let user: IUser | null

      try {
        user = await User.findById(_id).exec()
      } catch (error) {
        console.error('> getUser error: ', error)

        throw new ApolloError('Error retrieving user')
      }

      return user
    }
  },
  Mutation: {}
}
